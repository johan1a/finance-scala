FROM adoptopenjdk/openjdk11-openj9:jdk-11.0.1.13-alpine-slim

WORKDIR /app

ENV JAVA_TOOL_OPTIONS=-Dfile.encoding=UTF-8

COPY target/universal/finance-*.zip finance.zip

RUN set -x \
  && apk add bash \
  && unzip -d /app finance.zip \
  && mv /app/*/* /app/ \
  && rm /app/bin/*.bat

EXPOSE 8080
CMD /app/bin/finance -Dhttp.port=8080
