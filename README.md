# Finance

## Appendix

### Running

You need to download and install sbt for this application to run.

Once you have sbt installed, the following at the command prompt will start up Play in development mode:

```bash
sbt run
```

Play will start up on the HTTP port at <http://localhost:3003/>.   You don't need to deploy or reload anything -- changing any source code while the server is running will automatically recompile and hot-reload the application on the next HTTP request.

### Usage

If you call the same URL from the command line, you’ll see JSON. Using [httpie](https://httpie.org/), we can execute the command:

```bash
http --verbose http://localhost:3003/api/finance/transactions/
```

and get back:

```routes
GET /api/finance/transactions/ HTTP/1.1
```

Upload with httpie

```
http --form :3003/api/finance/transactions/files/ @the_file.csv Authorization:'Bearer thetoken'

```

Likewise, you can also send a POST directly as JSON:

```bash
http --verbose POST http://localhost:3003/api/finance/transactions/ title="hello" body="world"
```

and get:

```routes
POST /api/deployments HTTP/1.1
```

## Create keys

ssh-keygen -t rsa -b 4096
