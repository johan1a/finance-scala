package auth

import javax.inject.Inject
import java.time.Clock
import play.api.Configuration
import scala.util.{Failure, Success, Try}
import play.api.Logger
import java.nio.file.{Files, Paths, Path}
import java.nio.charset.StandardCharsets.UTF_8
import java.security._
import java.security.spec._
import java.security.interfaces._
import java.util.Base64
import com.auth0.jwt.algorithms._
import com.auth0.jwt._
import com.auth0.jwt.impl._
import com.auth0.jwt.interfaces._
import com.auth0.jwt.exceptions._

class AuthService @Inject()(config: Configuration) {

  private val logger = Logger(getClass)

  private val publicKeyPath = Paths.get(config.get[String]("public_key.path"))

  def loadPublicKey(path: Path, algorithm: String = "RSA"): RSAPublicKey = {
    val bytes = Files.readAllBytes(publicKeyPath)
    val spec  = new X509EncodedKeySpec(bytes)
    val kf    = KeyFactory.getInstance(algorithm)
    kf.generatePublic(spec).asInstanceOf[RSAPublicKey]
  }

  def getUserId(token: String): Long = {
    val jwt = verify(token)
    jwt.getClaim("user").asMap.get("id").toString.toLong
  }

  def hasAccess(token: String): Boolean = {
    var succeeded = false
    try {
      val jwt: DecodedJWT = verify(token)
      logger.info(jwt.toString)
      succeeded = true
    } catch {
      case (e: JWTVerificationException) => {
        logger.error("Token error: ", e)
      }
    }
    succeeded
  }

  private def verify(token: String): DecodedJWT = {
    val publicKey: RSAPublicKey = loadPublicKey(publicKeyPath)

    val algorithm               = Algorithm.RSA256(publicKey, null);
    val verifier = JWT
      .require(algorithm)
      .build()
    verifier.verify(token);
  }
}
