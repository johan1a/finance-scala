package auth

import play.api.mvc.Request

object HeaderHelper {

  def getBearerToken(request: Request[_]): Option[String] = {
    var result: Option[String] = None
    request.headers.get("Authorization").map { header =>
      if (header.toLowerCase.contains("bearer ")) {
        result = Some(header.split(" ")(1))
      }
    }
    result
  }

}
