package v1.account

import javax.inject.Inject

import net.logstash.logback.marker.LogstashMarker
import play.api.{Logger, MarkerContext}
import play.api.http.{FileMimeTypes, HttpVerbs}
import play.api.i18n.{Langs, MessagesApi}
import play.api.mvc._

import auth._
import scala.concurrent.{ExecutionContext, Future}

import play.api.mvc.Results._

trait AccountRequestHeader extends MessagesRequestHeader with PreferredMessagesProvider
class AccountRequest[A](request: Request[A], val messagesApi: MessagesApi, val userId: String)
    extends WrappedRequest(request)
    with AccountRequestHeader

trait RequestMarkerContext {
  import net.logstash.logback.marker.Markers

  private def marker(tuple: (String, Any)) = Markers.append(tuple._1, tuple._2)

  private implicit class RichLogstashMarker(marker1: LogstashMarker) {
    def &&(marker2: LogstashMarker): LogstashMarker = marker1.and(marker2)
  }

  implicit def requestHeaderToMarkerContext(
      implicit request: RequestHeader
  ): MarkerContext = {
    MarkerContext {
      marker("id"       -> request.id) && marker("host" -> request.host) && marker(
        "remoteAddress" -> request.remoteAddress
      )
    }
  }

}

class AccountActionBuilder @Inject()(
    messagesApi: MessagesApi,
    authService: AuthService,
    playBodyParsers: PlayBodyParsers
)(implicit val executionContext: ExecutionContext)
    extends ActionBuilder[AccountRequest, AnyContent]
    with RequestMarkerContext
    with HttpVerbs {

  override val parser: BodyParser[AnyContent] = playBodyParsers.anyContent

  type AccountRequestBlock[A] = AccountRequest[A] => Future[Result]

  private val logger = Logger(this.getClass)

  override def invokeBlock[A](
      request: Request[A],
      block: AccountRequestBlock[A]
  ): Future[Result] = {
    val token = HeaderHelper.getBearerToken(request)
    if (token.isEmpty || !authService.hasAccess(token.get)) {
      Future {
        Forbidden("")
      }
    } else {
      val userId: String                          = authService.getUserId(token.get).toString
      implicit val markerContext: MarkerContext = requestHeaderToMarkerContext(request)
      val future = block(new AccountRequest(request, messagesApi, userId))

      future.map { result =>
        request.method match {
          case GET | HEAD =>
            result.withHeaders("Cache-Control" -> s"max-age: 100")
          case other =>
            result
        }
      }
    }
  }
}

case class AccountControllerComponents @Inject()(
    accountActionBuilder: AccountActionBuilder,
    accountResourceHandler: AccountResourceHandler,
    actionBuilder: DefaultActionBuilder,
    parsers: PlayBodyParsers,
    messagesApi: MessagesApi,
    langs: Langs,
    fileMimeTypes: FileMimeTypes,
    executionContext: scala.concurrent.ExecutionContext
) extends ControllerComponents

/**
  * Exposes actions and handler to the AccountController by wiring the injected state into the base class.
  */
class AccountBaseController @Inject()(pcc: AccountControllerComponents)
    extends BaseController
    with RequestMarkerContext {
  override protected def controllerComponents: ControllerComponents = pcc

  def AccountAction: AccountActionBuilder = pcc.accountActionBuilder

  def accountResourceHandler: AccountResourceHandler =
    pcc.accountResourceHandler
}
