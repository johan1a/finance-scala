package v1.account

import javax.inject.Inject

import scala.collection.mutable
import play.api.Logger
import play.api.data.Form
import play.api.libs.json.Json
import play.api.mvc._
import java.io.FileReader
import org.apache.commons.csv._

import play.api.libs.json._

import scala.concurrent.{ExecutionContext, Future}
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import play.api.libs.Files

case class Account(
    name: String,
    userId: Option[Long] = None
)

object Account {

  implicit val format: Format[Account] = Json.format
}

class AccountController @Inject()(cc: AccountControllerComponents)(
    implicit ec: ExecutionContext
) extends AccountBaseController(cc) {

  private val logger = Logger(getClass)

  implicit val accountReads: Reads[Account]   = Json.reads[Account]
  implicit val accountWrites: Writes[Account] = Json.writes[Account]

  def index(userId: String): Action[AnyContent] = AccountAction.async { implicit request =>
    logger.info(s"index: userId: $userId, request.userId: ${request.userId}")
    if (userId != request.userId.toString) {
      Future {
        Forbidden
      }
    } else {
      accountResourceHandler.list(userId).map(accounts => Ok(Json.toJson(accounts)))
    }
  }

  def show(userId: String, accountId: String): Action[AnyContent] = AccountAction.async {
    implicit request =>
      logger.info(s"show: userId: $userId, accountId: $accountId request.userId: ${request.userId}")
      if (userId != request.userId.toString) {
        Future {
          Forbidden
        }
      } else {
        accountResourceHandler.get(AccountId(accountId)).map { maybeAccount =>
          maybeAccount match {
            case Some(account) => Ok(Json.toJson(account))
            case None          => NotFound
          }
        }
      }
  }

  def create(userId: String): Action[AnyContent] = AccountAction.async { implicit request =>
    logger.info("create")
    if (userId != request.userId.toString) {
      Future {
        Forbidden
      }
    } else {
      val json = request.body.asJson.get
      logger.trace("got json: " + json.toString)
      val input: Account = json.as[Account]
      accountResourceHandler.create(input, request.userId).map { accountId =>
        logger.info(s"created: $accountId")
        Created(Json.toJson(accountId.toString))
      }
    }
  }

}
