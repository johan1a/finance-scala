package v1.account

import javax.inject.{Inject, Singleton}

import akka.actor.ActorSystem
import play.api.libs.concurrent.CustomExecutionContext
import play.api.{Logger, MarkerContext}

import scala.concurrent.Future
import v1.account._
import org.mongodb.scala._
import org.mongodb.scala.bson.ObjectId
import scala.collection.JavaConverters._
import com.mongodb.MongoCredential._
import java.util.concurrent.TimeUnit
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import org.mongodb.scala.model.Filters._
import play.api.libs.json._
import play.api.Configuration

final case class AccountData(
    id: Option[AccountId],
    userId: String,
    name: String
)

class AccountId private (val underlying: String) extends AnyVal {
  override def toString: String = underlying.toString
}

object AccountId {
  def apply(raw: String): AccountId = {
    require(raw != null)
    new AccountId(raw)
  }
}

class AccountExecutionContext @Inject()(actorSystem: ActorSystem)
    extends CustomExecutionContext(actorSystem, "repository.dispatcher")

trait AccountRepository {
  def list(userId: String)(implicit mc: MarkerContext): Future[Iterable[AccountData]]

  def create(accountData: AccountData)(implicit mc: MarkerContext): Future[AccountId]

  def get(id: AccountId)(implicit mc: MarkerContext): Future[Option[AccountData]]
}

@Singleton
class AccountRepositoryImpl @Inject()(config: Configuration)(
    implicit ec: AccountExecutionContext
) extends AccountRepository {

  private val logger = Logger(this.getClass)

  val user: String                = config.get[String]("db.user")
  val sourceDatabase: String      = config.get[String]("db.name")
  val password: Array[Char]       = config.get[String]("db.password").toCharArray
  val host: String                = config.get[String]("db.host")
  val credential: MongoCredential = createCredential(user, sourceDatabase, password)

  val settings: MongoClientSettings = MongoClientSettings
    .builder()
    .applyToClusterSettings(b => b.hosts(List(new ServerAddress(host)).asJava))
    .credential(credential)
    .build()

  val mongoClient: MongoClient = MongoClient(settings)

  val database: MongoDatabase = mongoClient.getDatabase(sourceDatabase)

  val collection = database.getCollection("account")

  override def list(userId: String)(implicit mc: MarkerContext): Future[Iterable[AccountData]] = {
    collection
      .find(equal("userId", userId))
      .toFuture
      .map { documents =>
        documents.map { createAccountDocument(_) }
      }
  }

  override def create(
      accountData: AccountData
  )(implicit mc: MarkerContext): Future[AccountId] = {
    logger.trace(s"create: accountData = $accountData")
    val id         = new ObjectId()
    val observable = collection.insertOne(createAccountDocument(id, accountData))
    observable.toFuture.map { completed =>
      AccountId(id.toString)
    }
  }

  override def get(
      id: AccountId
  )(implicit mc: MarkerContext): Future[Option[AccountData]] = {
    logger.trace(s"get: id = $id")
    collection.find(equal("_id", new ObjectId(id.toString))).first().toFuture.map { document =>
      if (document == null) None else Some(createAccountDocument(document))
    }
  }

  private def createAccountDocument(document: Document): AccountData = {
    AccountData(
      Some(AccountId(document.getObjectId("_id").toString)),
      document.getString("userId"),
      document.getString("name")
    )
  }

  private def maybeAccountId(rawId: String): Option[AccountId] = {
    if (rawId == "None" || rawId == "") None else Some(AccountId(rawId))
  }

  private def createAccountDocument(
      id: ObjectId,
      accountData: AccountData
  ): Document = {
    Document(
      "_id"    -> id,
      "userId" -> accountData.userId,
      "name"   -> accountData.name
    )
  }

}
