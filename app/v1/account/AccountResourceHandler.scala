package v1.account

import java.io.FileReader

import javax.inject.{Inject, Provider}
import play.api.{Logger, MarkerContext}
import play.api.libs.json._
import org.mongodb.scala._
import scala.concurrent.{ExecutionContext, Future}
import v1.account._
import org.mongodb.scala.bson.ObjectId

/**
  * DTO for displaying account information.
  */
case class AccountResource(
    id: Option[String],
    userId: String,
    name: String
)

object AccountResource {

  /**
    * Mapping to read/write a AccountResource out as a JSON value.
    */
  implicit val format: Format[AccountResource] = Json.format
}

class AccountResourceHandler @Inject()(
    accountRepository: AccountRepository
)(implicit ec: ExecutionContext) {

  private val logger = Logger(getClass)

  def get(id: AccountId): Future[Option[AccountResource]] = {
    logger.info(s"id: $id")
    accountRepository.get(id).map { maybeAccountData =>
      maybeAccountData.map { accountData =>
        createAccountResource(accountData)
      }
    }
  }

  def list(userId: String): Future[Iterable[AccountResource]] = {
    logger.info("list called")
    accountRepository.list(userId).map { accounts =>
      accounts.map { account =>
        createAccountResource(account)
      }
    }
  }

  def create(account: Account, userId: String): Future[AccountId] = {
    val data = mapAccount(account, userId)
    accountRepository.create(data).map { accountId =>
      accountId
    }
  }

  def mapAccount(account: Account, userId: String): AccountData = {
    AccountData(
      None,
      userId,
      account.name
    )
  }

  private def createAccountResource(accountData: AccountData): AccountResource = {
    AccountResource(
      accountData.id.map(_.toString),
      accountData.userId,
      accountData.name
    )
  }

}
