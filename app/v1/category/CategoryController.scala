package v1.category

import javax.inject.Inject

import scala.collection.mutable
import play.api.Logger
import play.api.data.Form
import play.api.libs.json.Json
import play.api.mvc._
import java.io.FileReader
import org.apache.commons.csv._

import play.api.libs.json._

import scala.concurrent.{ExecutionContext, Future}
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import play.api.libs.Files

case class Category(description: String, userId: Option[String], parentCategoryId: Option[String])

object Category {

  implicit val format: Format[Category] = Json.format
}

class CategoryController @Inject()(cc: CategoryControllerComponents)(
    implicit ec: ExecutionContext
) extends CategoryBaseController(cc) {

  private val logger = Logger(getClass)

  implicit val categoryReads: Reads[Category]   = Json.reads[Category]
  implicit val categoryWrites: Writes[Category] = Json.writes[Category]

  def index(userId: String): Action[AnyContent] = CategoryAction.async { implicit request =>
    logger.info(s"index: userId: $userId, request.userId: ${request.userId}")
    if (userId != request.userId.toString) {
      Future {
        Forbidden
      }
    } else {
      categoryResourceHandler.list(userId).map(categories => Ok(Json.toJson(categories)))
    }
  }

  def show(userId: String, id: String): Action[AnyContent] = CategoryAction.async {
    implicit request =>
      logger.info(s"show: id: $id, userId: $userId, request.userId: ${request.userId}")
      if (userId != request.userId.toString) {
        Future {
          Forbidden
        }
      } else {
        logger.trace(s"show: id = $id")
        categoryResourceHandler.get(CategoryId(id)).map { maybeCategory =>
          maybeCategory match {
            case Some(category) => Ok(Json.toJson(category))
            case None           => NotFound
          }
        }
      }
  }

  def create(userId: String): Action[AnyContent] = CategoryAction.async { implicit request =>
    logger.info("create")
    if (userId != request.userId.toString) {
      Future {
        Forbidden
      }
    } else {
      val json = request.body.asJson.get
      logger.info("got json: " + json.toString)
      val input: Category = json.as[Category]
      categoryResourceHandler.create(input, request.userId).map { categoryId =>
        logger.info(s"created: $categoryId")
        Created(Json.toJson(categoryId.toString))
      }
    }
  }

}
