package v1.category

import javax.inject.{Inject, Singleton}

import akka.actor.ActorSystem
import play.api.libs.concurrent.CustomExecutionContext
import play.api.{Logger, MarkerContext}

import scala.concurrent.Future
import v1.account._
import v1.category._
import org.mongodb.scala._
import org.mongodb.scala.bson.ObjectId
import scala.collection.JavaConverters._
import com.mongodb.MongoCredential._
import java.util.concurrent.TimeUnit
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import org.mongodb.scala.model.Filters._
import play.api.libs.json._
import play.api.Configuration

final case class CategoryData(
    id: Option[CategoryId],
    userId: String,
    description: String,
    parentCategoryId: Option[CategoryId]
)

class CategoryId private (val underlying: String) extends AnyVal {
  override def toString: String = underlying.toString
}

object CategoryId {
  def apply(raw: String): CategoryId = {
    require(raw != null)
    new CategoryId(raw)
  }

}

class CategoryExecutionContext @Inject()(actorSystem: ActorSystem)
    extends CustomExecutionContext(actorSystem, "repository.dispatcher")

trait CategoryRepository {
  def create(data: CategoryData)(implicit mc: MarkerContext): Future[CategoryId]

  def list(userId: String)(implicit mc: MarkerContext): Future[Iterable[CategoryData]]

  def get(id: CategoryId)(implicit mc: MarkerContext): Future[Option[CategoryData]]
}

@Singleton
class CategoryRepositoryImpl @Inject()(config: Configuration)(
    implicit ec: CategoryExecutionContext
) extends CategoryRepository {

  private val logger = Logger(this.getClass)

  val user: String                = config.get[String]("db.user")
  val sourceDatabase: String      = config.get[String]("db.name")
  val password: Array[Char]       = config.get[String]("db.password").toCharArray
  val host: String                = config.get[String]("db.host")
  val credential: MongoCredential = createCredential(user, sourceDatabase, password)

  val settings: MongoClientSettings = MongoClientSettings
    .builder()
    .applyToClusterSettings(b => b.hosts(List(new ServerAddress(host)).asJava))
    .credential(credential)
    .build()

  val mongoClient: MongoClient = MongoClient(settings)

  val database: MongoDatabase = mongoClient.getDatabase(sourceDatabase)

  val collection = database.getCollection("category")

  override def list(userId: String)(implicit mc: MarkerContext): Future[Iterable[CategoryData]] = {
    collection
      .find(equal("userId", userId))
      .toFuture
      .map { documents =>
        documents.map { createCategoryDocument(_) }
      }
  }

  override def get(
      id: CategoryId
  )(implicit mc: MarkerContext): Future[Option[CategoryData]] = {
    logger.trace(s"get: id = $id")
    collection.find(equal("_id", new ObjectId(id.toString))).first().toFuture.map { document =>
      if (document == null) None else Some(createCategoryDocument(document))
    }
  }

  def create(categoryData: CategoryData)(implicit mc: MarkerContext): Future[CategoryId] = {
    logger.trace(s"create: categoryData = $categoryData")
    val id         = new ObjectId()
    val observable = collection.insertOne(createCategoryDocument(id, categoryData))
    observable.toFuture.map { completed =>
      CategoryId(id.toString)
    }
  }

  private def createCategoryDocument(document: Document): CategoryData = {
    val parentId = document.getString("parentCategoryId")
    CategoryData(
      Some(CategoryId(document.getObjectId("_id").toString)),
      document.getString("userId"),
      document.getString("description"),
      if (parentId != null) Some(CategoryId(parentId)) else None
    )
  }

  private def maybeAccountId(rawId: String): Option[AccountId] = {
    if (rawId == "None" || rawId == "") None else Some(AccountId(rawId))
  }

  private def createCategoryDocument(
      id: ObjectId,
      categoryData: CategoryData
  ): Document = {
    Document(
      "_id"         -> id,
      "userId"      -> categoryData.userId,
      "description" -> categoryData.description,
      "parentCategoryId" -> categoryData.parentCategoryId.map { id =>
        id.toString
      }
    )
  }

}
