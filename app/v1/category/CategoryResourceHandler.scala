package v1.category

import java.io.FileReader

import javax.inject.{Inject, Provider}
import play.api.{Logger, MarkerContext}
import play.api.libs.json._
import org.mongodb.scala._
import scala.concurrent.{ExecutionContext, Future}
import v1.category._
import v1.account.AccountId
import org.mongodb.scala.bson.ObjectId

/**
  * DTO for displaying category information.
  */
case class CategoryResource(
    id: Option[String],
    userId: String,
    description: String,
    parentCategoryId: Option[String]
)

object CategoryResource {

  /**
    * Mapping to read/write a CategoryResource out as a JSON value.
    */
  implicit val format: Format[CategoryResource] = Json.format
}

/**
  * Controls access to the backend data, returning [[CategoryResource]]
  */
class CategoryResourceHandler @Inject()(
    categoryRepository: CategoryRepository
)(implicit ec: ExecutionContext) {

  private val logger = Logger(getClass)

  def get(id: CategoryId): Future[Option[CategoryResource]] = {
    logger.info(s"id: $id")
    categoryRepository.get(id).map { maybeCategoryData =>
      maybeCategoryData.map { categoryData =>
        createCategoryResource(categoryData)
      }
    }
  }

  def list(userId: String): Future[Iterable[CategoryResource]] = {
    logger.info("list called")
    categoryRepository.list(userId).map { categories =>
      categories.map { category =>
        createCategoryResource(category)
      }
    }
  }

  def create(category: Category, userId: String): Future[CategoryId] = {
    val data = mapCategory(category, userId)
    categoryRepository.create(data).map { categoryId =>
      categoryId
    }
  }

  def mapCategory(category: Category, userId: String): CategoryData = {
    CategoryData(
      None,
      userId,
      category.description,
      category.parentCategoryId.map { id =>
        CategoryId(id)
      }
    )
  }

  def createCategoryResource(categoryData: CategoryData): CategoryResource = {
    CategoryResource(
      categoryData.id.map(_.toString),
      categoryData.userId,
      categoryData.description,
      categoryData.parentCategoryId.map { id =>
        id.toString
      }
    )
  }

}
