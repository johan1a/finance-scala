package v1.transaction

import javax.inject.Inject

import net.logstash.logback.marker.LogstashMarker
import play.api.{Logger, MarkerContext}
import play.api.http.{FileMimeTypes, HttpVerbs}
import play.api.i18n.{Langs, MessagesApi}
import play.api.mvc._

import auth._
import scala.concurrent.{ExecutionContext, Future}

import play.api.mvc.Results._

/**
  * A wrapped request for transaction resources.
  *
  * This is commonly used to hold request-specific information like
  * security credentials, and useful shortcut methods.
  */
trait TransactionRequestHeader extends MessagesRequestHeader with PreferredMessagesProvider
class TransactionRequest[A](request: Request[A], val messagesApi: MessagesApi, val userId: String)
    extends WrappedRequest(request)
    with TransactionRequestHeader

trait RequestMarkerContext {
  import net.logstash.logback.marker.Markers

  private def marker(tuple: (String, Any)) = Markers.append(tuple._1, tuple._2)

  private implicit class RichLogstashMarker(marker1: LogstashMarker) {
    def &&(marker2: LogstashMarker): LogstashMarker = marker1.and(marker2)
  }

  implicit def requestHeaderToMarkerContext(
      implicit request: RequestHeader
  ): MarkerContext = {
    MarkerContext {
      marker("id"       -> request.id) && marker("host" -> request.host) && marker(
        "remoteAddress" -> request.remoteAddress
      )
    }
  }

}

/**
  * The action builder for the Transaction resource.
  *
  * This is the place to put logging, metrics, to augment
  * the request with contextual data, and manipulate the
  * result.
  */
class TransactionActionBuilder @Inject()(
    messagesApi: MessagesApi,
    authService: AuthService,
    playBodyParsers: PlayBodyParsers
)(implicit val executionContext: ExecutionContext)
    extends ActionBuilder[TransactionRequest, AnyContent]
    with RequestMarkerContext
    with HttpVerbs {

  override val parser: BodyParser[AnyContent] = playBodyParsers.anyContent

  type TransactionRequestBlock[A] = TransactionRequest[A] => Future[Result]

  private val logger = Logger(this.getClass)

  override def invokeBlock[A](
      request: Request[A],
      block: TransactionRequestBlock[A]
  ): Future[Result] = {
    val token = HeaderHelper.getBearerToken(request)
    if (token.isEmpty || !authService.hasAccess(token.get)) {
      Future {
        Forbidden("")
      }
    } else {
      val userId: String                          = authService.getUserId(token.get).toString
      implicit val markerContext: MarkerContext = requestHeaderToMarkerContext(request)
      logger.trace(s"invokeBlock: ")
      val future = block(new TransactionRequest(request, messagesApi, userId))

      future.map { result =>
        request.method match {
          case GET | HEAD =>
            result.withHeaders("Cache-Control" -> s"max-age: 100")
          case other =>
            result
        }
      }
    }
  }
}

/**
  * Packages up the component dependencies for the transaction controller.
  *
  * This is a good way to minimize the surface area exposed to the controller, so the
  * controller only has to have one thing injected.
  */
case class TransactionControllerComponents @Inject()(
    transactionActionBuilder: TransactionActionBuilder,
    transactionResourceHandler: TransactionResourceHandler,
    actionBuilder: DefaultActionBuilder,
    parsers: PlayBodyParsers,
    messagesApi: MessagesApi,
    langs: Langs,
    fileMimeTypes: FileMimeTypes,
    executionContext: scala.concurrent.ExecutionContext
) extends ControllerComponents

/**
  * Exposes actions and handler to the TransactionController by wiring the injected state into the base class.
  */
class TransactionBaseController @Inject()(pcc: TransactionControllerComponents)
    extends BaseController
    with RequestMarkerContext {
  override protected def controllerComponents: ControllerComponents = pcc

  def TransactionAction: TransactionActionBuilder = pcc.transactionActionBuilder

  def transactionResourceHandler: TransactionResourceHandler =
    pcc.transactionResourceHandler
}
