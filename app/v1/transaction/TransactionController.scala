package v1.transaction

import javax.inject.Inject

import scala.collection.mutable
import play.api.Logger
import play.api.data.Form
import play.api.libs.json.Json
import play.api.mvc._
import java.io.FileReader
import org.apache.commons.csv._

import play.api.libs.json._

import scala.concurrent.{ExecutionContext, Future}
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import play.api.libs.Files

case class Transaction(
    amount: Long,
    date: String,
    text: String,
    currency: String,
    userId: Option[String],
    categoryId: Option[String]
)

object Transaction {

  implicit val format: Format[Transaction] = Json.format
}

class TransactionController @Inject()(cc: TransactionControllerComponents)(
    implicit ec: ExecutionContext
) extends TransactionBaseController(cc) {

  private val logger = Logger(getClass)

  implicit val transactionReads: Reads[Transaction]   = Json.reads[Transaction]
  implicit val transactionWrites: Writes[Transaction] = Json.writes[Transaction]

  def index(userId: String): Action[AnyContent] = TransactionAction.async { implicit request =>
    logger.info("index: ")
    if (userId != request.userId.toString) {
      Future {
        Forbidden
      }
    } else {
      transactionResourceHandler
        .list(request.userId)
        .map(transactions => Ok(Json.toJson(transactions)))
    }
  }

  def show(userId: String, id: String): Action[AnyContent] = TransactionAction.async {
    implicit request =>
      logger.info(s"show: userId: $userId, transactionId: $id request.userId: ${request.userId}")
      if (userId != request.userId.toString) {
        Future {
          Forbidden
        }
      } else {
        transactionResourceHandler.get(TransactionId(id)).map { maybeTransaction =>
          maybeTransaction match {
            case Some(transaction) => Ok(Json.toJson(transaction))
            case None              => NotFound
          }
        }
      }
  }

  def create(userId: String): Action[AnyContent] = TransactionAction.async { implicit request =>
    logger.info(s"show: userId: $userId, request.userId: ${request.userId}")
    if (userId != request.userId.toString) {
      Future {
        Forbidden
      }
    } else {
      val json = request.body.asJson.get
      logger.info("got json: " + json.toString)
      val transaction: Transaction = json.as[Transaction]
      transactionResourceHandler.create(transaction, request.userId).map { transactionId =>
        logger.info(s"created: $transactionId")
        Created(Json.toJson(transactionId.toString))
      }
    }
  }

  def createFromCsv(userId: String): Action[AnyContent] = TransactionAction.async {
    implicit request =>
      logger.info(s"show: userId: $userId, request.userId: ${request.userId}")
      if (userId != request.userId.toString) {
        Future {
          Forbidden
        }
      } else {
        val maybeFormData = request.body.asMultipartFormData
        logger.info("createFromCsv maybeFormData: " + maybeFormData)
        val transactions = parseCsv(maybeFormData, request.userId)
        transactionResourceHandler.create(transactions, request.userId).map { transactions =>
          logger.info(s"created: $transactions")
          Created(Json.toJson(transactions))
        }
      }
  }

  private def parseCsv(
      maybeFormData: Option[MultipartFormData[Files.TemporaryFile]],
      userId: String
  ): Seq[Transaction] = {
    val transactions = mutable.Queue[Transaction]()
    maybeFormData.map { formData =>
      val filePath = formData.files.head.ref.path.toString
      val in       = new FileReader(filePath)
      val records  = CSVFormat.newFormat(';').withQuote('"').withHeader().parse(in)

      records.forEach { record: CSVRecord =>
        val b = record.get("Beløb").trim()
        println(b)
        transactions += Transaction(
          (BigDecimal(record.get("Beløb").trim()) * 100).toLong,
          record.get("Dato"),
          record.get("Tekst"),
          record.get("Valuta"),
          Some(userId),
          None
        )
      }
    }
    transactions
  }

}
