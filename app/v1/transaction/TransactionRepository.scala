package v1.transaction

import javax.inject.{Inject, Singleton}

import akka.actor.ActorSystem
import play.api.libs.concurrent.CustomExecutionContext
import play.api.{Logger, MarkerContext}

import scala.concurrent.Future
import v1.account._
import v1.category._
import org.mongodb.scala._
import org.mongodb.scala.bson.ObjectId
import scala.collection.JavaConverters._
import com.mongodb.MongoCredential._
import java.util.concurrent.TimeUnit
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import org.mongodb.scala.model.Filters._
import play.api.libs.json._
import play.api.Configuration

final case class TransactionData(
    id: Option[TransactionId],
    userId: String,
    accountId: Option[AccountId],
    destinationAccountId: Option[AccountId],
    amount: Long,
    date: String,
    text: String,
    currency: String,
    categoryId: Option[CategoryId]
)

class TransactionId private (val underlying: String) extends AnyVal {
  override def toString: String = underlying.toString
}

object TransactionId {
  def apply(raw: String): TransactionId = {
    require(raw != null)
    new TransactionId(raw)
  }

}

class TransactionExecutionContext @Inject()(actorSystem: ActorSystem)
    extends CustomExecutionContext(actorSystem, "repository.dispatcher")

trait TransactionRepository {
  def create(data: TransactionData)(implicit mc: MarkerContext): Future[TransactionId]

  def create(transactionDataSeq: Seq[TransactionData])(
      implicit mc: MarkerContext
  ): Future[Seq[TransactionId]]

  def list(userId: String)(implicit mc: MarkerContext): Future[Iterable[TransactionData]]

  def get(id: TransactionId)(implicit mc: MarkerContext): Future[Option[TransactionData]]
}

@Singleton
class TransactionRepositoryImpl @Inject()(config: Configuration)(
    implicit ec: TransactionExecutionContext
) extends TransactionRepository {

  private val logger = Logger(this.getClass)

  val user: String                = config.get[String]("db.user")
  val sourceDatabase: String      = config.get[String]("db.name")
  val password: Array[Char]       = config.get[String]("db.password").toCharArray
  val host: String                = config.get[String]("db.host")
  val credential: MongoCredential = createCredential(user, sourceDatabase, password)

  val settings: MongoClientSettings = MongoClientSettings
    .builder()
    .applyToClusterSettings(b => b.hosts(List(new ServerAddress(host)).asJava))
    .credential(credential)
    .build()

  val mongoClient: MongoClient = MongoClient(settings)

  val database: MongoDatabase = mongoClient.getDatabase(sourceDatabase)

  val collection = database.getCollection("transaction")

  override def list(
      userId: String
  )(implicit mc: MarkerContext): Future[Iterable[TransactionData]] = {
    collection
      .find(equal("userId", userId))
      .toFuture
      .map { documents =>
        documents.map { createTransactionDocument(_) }
      }
  }

  override def get(
      id: TransactionId
  )(implicit mc: MarkerContext): Future[Option[TransactionData]] = {
    logger.trace(s"get: id = $id")
    collection.find(equal("_id", new ObjectId(id.toString))).first().toFuture.map { document =>
      if (document == null) None else Some(createTransactionDocument(document))
    }
  }

  def create(
      transactionData: TransactionData
  )(implicit mc: MarkerContext): Future[TransactionId] = {
    logger.trace(s"create: transactionData = $transactionData")
    val id         = new ObjectId()
    val observable = collection.insertOne(createTransactionDocument(id, transactionData))
    observable.toFuture.map { completed =>
      TransactionId(id.toString)
    }
  }

  def create(
      transactionDataSeq: Seq[TransactionData]
  )(implicit mc: MarkerContext): Future[Seq[TransactionId]] = {

    val ids = transactionDataSeq.map { _ =>
      new ObjectId()
    }

    val documents = transactionDataSeq.zip(ids).map { dataAndId =>
      createTransactionDocument(dataAndId._2, dataAndId._1)
    }

    val observable = collection.insertMany(documents)
    observable.toFuture.map { completed =>
      logger.trace(s"created data: = $transactionDataSeq")
      ids.map { uuid =>
        TransactionId(uuid.toString)
      }
    }
  }

  private def createTransactionDocument(document: Document): TransactionData = {
    val categoryId = document.getString("categoryId")
    TransactionData(
      Some(TransactionId(document.getObjectId("_id").toString)),
      document.getString("userId"),
      maybeAccountId(document.getString("accountId")),
      maybeAccountId(document.getString("destinationAccountId")),
      document.getLong("amount"),
      document.getString("date"),
      document.getString("text"),
      document.getString("currency"),
      if (categoryId != null) Some(CategoryId(categoryId)) else None
    )
  }

  private def maybeAccountId(rawId: String): Option[AccountId] = {
    if (rawId == "None" || rawId == "") None else Some(AccountId(rawId))
  }

  private def createTransactionDocument(
      id: ObjectId,
      transactionData: TransactionData
  ): Document = {
    Document(
      "_id"                  -> id,
      "userId"               -> transactionData.userId,
      "accountId"            -> transactionData.accountId.toString,
      "destinationAccountId" -> transactionData.destinationAccountId.getOrElse("").toString,
      "amount"               -> transactionData.amount,
      "date"                 -> transactionData.date,
      "text"                 -> transactionData.text,
      "currency"             -> transactionData.currency,
      "categoryId"           -> transactionData.categoryId.getOrElse("").toString
    )
  }

}
