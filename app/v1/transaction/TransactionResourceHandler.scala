package v1.transaction

import java.io.FileReader

import javax.inject.{Inject, Provider}
import play.api.{Logger, MarkerContext}
import play.api.libs.json._
import org.mongodb.scala._
import scala.concurrent.{ExecutionContext, Future}
import v1.category._
import org.mongodb.scala.bson.ObjectId

case class TransactionResource(
    id: Option[String],
    userId: String,
    accountId: Option[String],
    destinationAccountId: Option[String],
    amount: Long,
    date: String,
    text: String,
    currency: String,
    categoryId: Option[String]
)

object TransactionResource {

  /**
    * Mapping to read/write a TransactionResource out as a JSON value.
    */
  implicit val format: Format[TransactionResource] = Json.format
}

/**
  * Controls access to the backend data, returning [[TransactionResource]]
  */
class TransactionResourceHandler @Inject()(
    transactionRepository: TransactionRepository,
    categoryResourceHandler: CategoryResourceHandler
)(implicit ec: ExecutionContext) {

  private val logger = Logger(getClass)

  def get(id: TransactionId): Future[Option[TransactionResource]] = {
    logger.info(s"id: $id")
    transactionRepository.get(id).map { maybeTransactionData =>
      maybeTransactionData.map { transactionData =>
        createTransactionResource(transactionData)
      }
    }
  }

  def list(userId: String): Future[Iterable[TransactionResource]] = {
    logger.trace("list called")
    transactionRepository.list(userId).map { transactions =>
      transactions.map { transaction =>
        createTransactionResource(transaction)
      }
    }
  }

  def create(transaction: Transaction, userId: String): Future[TransactionId] = {
    val data = mapTransaction(transaction, userId)
    transactionRepository.create(data).map { transactionId =>
      transactionId
    }
  }

  def create(transactions: Seq[Transaction], userId: String): Future[Seq[TransactionResource]] = {
    val transactionDataSeq = transactions.map(t => mapTransaction(t, userId))
    transactionRepository.create(transactionDataSeq).map { ids =>
      transactionDataSeq.map { createTransactionResource(_) }
    }
  }

  def mapTransaction(transaction: Transaction, userId: String): TransactionData = {
    TransactionData(
      None,
      userId,
      None,
      None,
      transaction.amount.toLong,
      transaction.date,
      transaction.text,
      transaction.currency,
      transaction.categoryId.map(CategoryId(_))
    )
  }

  private def createTransactionResource(transactionData: TransactionData): TransactionResource = {
    TransactionResource(
      transactionData.id.map(_.toString),
      transactionData.userId,
      transactionData.accountId.map(_.toString),
      transactionData.destinationAccountId.map(_.toString),
      transactionData.amount,
      transactionData.date,
      transactionData.text,
      transactionData.currency,
      transactionData.categoryId.map(_.toString)
    )
  }

}
