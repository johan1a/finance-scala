import sbt.Keys._
import play.sbt.PlaySettings

lazy val root = (project in file("."))
  .enablePlugins(PlayService, PlayLayoutPlugin, Common)
  .settings(
    name := "finance",
    version := "2.7.x",
    scalaVersion := "2.12.8",
    PlayKeys.playDefaultPort := 3003,
    libraryDependencies ++= Seq(
      guice,
      ws,
      "com.auth0"              % "java-jwt"                        % "3.9.0",
      "org.apache.commons"     % "commons-csv"                     % "1.7",
      "org.joda"               % "joda-convert"                    % "2.1.2",
      "net.logstash.logback"   % "logstash-logback-encoder"        % "5.2",
      "io.lemonlabs"           %% "scala-uri"                      % "1.4.10",
      "net.codingwell"         %% "scala-guice"                    % "4.2.5",
      "org.scalatestplus.play" %% "scalatestplus-play"             % "4.0.3" % Test,
      "com.lihaoyi"            %% "upickle"                        % "0.7.1",
      "org.mockito"            % "mockito-core"                    % "3.2.0" % "test",
      "org.mongodb.scala"      %% "mongo-scala-driver"             % "2.6.0",
      "com.dimafeng"           %% "testcontainers-scala-scalatest" % "0.34.3" % "test"
    )
  )



lazy val gatlingVersion = "3.1.3"
lazy val gatling = (project in file("gatling"))
  .enablePlugins(GatlingPlugin)
  .settings(
    scalaVersion := "2.12.8",
    libraryDependencies ++= Seq(
      "io.gatling.highcharts" % "gatling-charts-highcharts" % gatlingVersion % Test,
      "io.gatling"            % "gatling-test-framework"    % gatlingVersion % Test
    )
  )

// Documentation for this project:
//    sbt "project docs" "~ paradox"
//    open docs/target/paradox/site/index.html
lazy val docs = (project in file("docs"))
  .enablePlugins(ParadoxPlugin)
  .settings(
    scalaVersion := "2.13.0",
    paradoxProperties += ("download_url" -> "https://example.lightbend.com/v1/download/play-rest-api")
  )
initialize := {
  val _         = initialize.value // run the previous initialization
  val supported = Set("1.8", "9", "10", "11", "12", "13")
  val current   = sys.props("java.specification.version")
  assert(
    supported.contains(current),
    s"Unsupported JDK: java.specification.version $current != $supported"
  )
}
