package v1.account

import play.api.libs.json.{JsResult, Json}
import org.mockito.{ArgumentMatchers, Matchers, Mockito}
import org.scalatest.TestData
import org.scalatestplus.mockito._
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.mvc.Result
import play.api.test.CSRFTokenHelper._
import play.api.test.Helpers._
import play.api.test._

import scala.concurrent.Future
import scala.language.implicitConversions

import v1.category._
import v1.account._
import org.apache.commons.io.IOUtils
import java.nio.charset.StandardCharsets
import play.api.mvc.MultipartFormData
import play.api.libs.Files.TemporaryFile
import play.api.mvc.MultipartFormData.FilePart
import play.api.mvc.MultipartFormData.BadPart
import play.api.libs.Files
import java.io.File
import play.api.libs.Files.SingletonTemporaryFileCreator
import java.nio.file.Paths
import java.nio.charset.StandardCharsets.UTF_8
import java.util.UUID
import org.mongodb.scala.bson.ObjectId
import com.dimafeng.testcontainers._
import org.testcontainers.containers.wait.strategy._

class AccountRouterSpec extends PlaySpec with GuiceOneAppPerTest with MockitoSugar {

  val csvFilePath = "/api/finance/accounts/files/"

  val mongoPort = 27017

  val dbUser     = "finance_test_app"
  val dbPassword = "finance_test_pw"
  val dbName     = "finance_test_db"

  val container = GenericContainer(
    "bitnami/mongodb:latest",
    exposedPorts = Seq(mongoPort),
    waitStrategy = Wait.forHttp("/"),
    env = Map(
      "MONGODB_USERNAME" -> dbUser,
      "MONGODB_PASSWORD" -> dbPassword,
      "MONGODB_DATABASE" -> dbName
    )
  )

  container.start()

  val mappedPort  = container.mappedPort(mongoPort)
  val containerIp = container.containerIpAddress

  println(s"mappedPort: ${mappedPort}, containerIp: ${containerIp}")
  implicit override def newAppForTest(testData: TestData): Application =
    new GuiceApplicationBuilder()
      .configure("db.user" -> dbUser)
      .configure("db.password" -> dbPassword)
      .configure("db.name" -> dbName)
      .configure("db.host" -> s"${containerIp}:${mappedPort}")
      .build()

  val testId        = new ObjectId().toString
  val jwtToken      = readFileAsString("/jwt_token.txt")
  val testUserId    = "6" // taken from jwt_token.txt
  val anotherUserId = 6123L
  val bearerToken   = s"Bearer ${jwtToken}"

  private def listPath(userId: String)   = s"/api/finance/users/${userId}/accounts/"
  private def createPath(userId: String) = listPath(userId)

  def showPath(userId: String, accountId: String) =
    s"/api/finance/users/${userId}/accounts/${accountId}/"

  val accounts = Vector(
    AccountResource(Some(testId), testUserId, "3")
  )

  "AccountRouter" should {

    "not allow unauthenticated requests" in {
      val request = FakeRequest(GET, s"${listPath(testUserId.toString)}")
        .withHeaders(HOST -> "localhost:9000")
        .withCSRFToken
      val home: Future[Result] = route(app, request).get
      val response             = await(home)
      response.header.status mustBe FORBIDDEN
    }

    "not allow indexing other users accounts" in {
      val request = FakeRequest(GET, s"${listPath(anotherUserId.toString)}")
        .withHeaders(HOST -> "localhost:9000", AUTHORIZATION -> bearerToken)
        .withCSRFToken
      val home: Future[Result] = route(app, request).get
      val response             = await(home)
      response.header.status mustBe FORBIDDEN
    }

    "not allow showing other users' accounts" in {
      val request = FakeRequest(GET, s"${showPath(anotherUserId.toString, "someId")}")
        .withHeaders(HOST -> "localhost:9000", AUTHORIZATION -> bearerToken)
        .withCSRFToken
      val home: Future[Result] = route(app, request).get
      val response             = await(home)
      response.header.status mustBe FORBIDDEN
    }

    "not allow creating accounts for other users" in {
      val account = Account("An account")
      val request = FakeRequest(POST, s"${listPath(anotherUserId.toString)}")
        .withHeaders(HOST -> "localhost:9000", AUTHORIZATION -> bearerToken)
        .withJsonBody(Json.toJson(account))
        .withCSRFToken
      val home: Future[Result] = route(app, request).get
      val response             = await(home)
      response.header.status mustBe FORBIDDEN
    }

    "create an Account" in {
      val input = Account("An account")
      val id    = createAccount(testUserId.toString, input)
      (id == "") mustBe false
    }

    "get an Account" in {
      val createInput = Account("account name")
      val id          = createAccount(testUserId.toString, createInput)
      (id == "") mustBe false

      val getRequest = FakeRequest(GET, showPath(testUserId.toString, id))
        .withHeaders(HOST -> "localhost:9000", AUTHORIZATION -> bearerToken)
        .withCSRFToken
      val get: Future[Result] = route(app, getRequest).get

      val account: AccountResource =
        Json.fromJson[AccountResource](contentAsJson(get)).get
      println(account)
      account.id mustBe (Some(id))
      account.userId mustBe testUserId
      account.name mustBe (createInput.name)
    }

    "list Accounts" in {
      val createInput0 = Account("account A")
      val id0          = createAccount(testUserId.toString, createInput0)

      val createInput1 = Account("account B")
      val id1          = createAccount(testUserId.toString, createInput1)

      val getRequest = FakeRequest(GET, listPath(testUserId.toString))
        .withHeaders(HOST -> "localhost:9000", AUTHORIZATION -> bearerToken)
        .withCSRFToken
      val get: Future[Result] = route(app, getRequest).get

      val accounts: Seq[AccountResource] =
        Json.fromJson[Seq[AccountResource]](contentAsJson(get)).get
      println(accounts)
      accounts.size >= 2
      val maybeAccount0 = accounts.find { _.id == Some(id0) }
      val maybeAccount1 = accounts.find { _.id == Some(id1) }

      maybeAccount0.nonEmpty mustBe true
      maybeAccount1.nonEmpty mustBe true

      val account0 = maybeAccount0.get
      val account1 = maybeAccount1.get

      account0.id mustBe (Some(id0))
      account0.userId mustBe testUserId
      account0.name mustBe (createInput0.name)

      account1.id mustBe (Some(id1))
      account1.userId mustBe testUserId
      account1.name mustBe (createInput1.name)
    }
  }

  private def createAccount(userId: String, account: Account): String = {
    val createRequest = FakeRequest(POST, createPath(userId))
      .withHeaders(HOST -> "localhost:9000", AUTHORIZATION -> bearerToken)
      .withJsonBody(Json.toJson(account))
      .withCSRFToken
    val create: Future[Result] = route(app, createRequest).get

    val createResponse = await(create)
    createResponse.header.status mustBe CREATED
    val id = Json.fromJson[String](contentAsJson(create)).get
    id
  }

  def readFileAsString(path: String): String = {
    val uri = getClass.getResource(path).toURI()
    new String(java.nio.file.Files.readAllBytes(Paths.get(uri)), UTF_8)
  }

}
