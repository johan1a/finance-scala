package v1.category

import play.api.libs.json.{JsResult, Json}
import org.mockito.{ArgumentMatchers, Matchers, Mockito}
import org.scalatest.TestData
import org.scalatestplus.mockito._
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.mvc.Result
import play.api.test.CSRFTokenHelper._
import play.api.test.Helpers._
import play.api.test._

import scala.concurrent.Future
import scala.language.implicitConversions

import v1.category._
import v1.category._
import org.apache.commons.io.IOUtils
import java.nio.charset.StandardCharsets
import play.api.mvc.MultipartFormData
import play.api.libs.Files.TemporaryFile
import play.api.mvc.MultipartFormData.FilePart
import play.api.mvc.MultipartFormData.BadPart
import play.api.libs.Files
import java.io.File
import play.api.libs.Files.SingletonTemporaryFileCreator
import java.nio.file.Paths
import java.nio.charset.StandardCharsets.UTF_8
import java.util.UUID
import org.mongodb.scala.bson.ObjectId
import com.dimafeng.testcontainers._
import org.testcontainers.containers.wait.strategy._

class CategoryRouterSpec extends PlaySpec with GuiceOneAppPerTest with MockitoSugar {

  val mongoPort = 27017

  val dbUser = "finance_test_app"
  val dbPassword = "finance_test_pw"
  val dbName = "finance_test_db"

  val container = GenericContainer(
    "bitnami/mongodb:latest",
    exposedPorts = Seq(mongoPort),
    waitStrategy = Wait.forHttp("/"),
    env = Map("MONGODB_USERNAME" -> dbUser,
              "MONGODB_PASSWORD" -> dbPassword,
              "MONGODB_DATABASE" -> dbName
    )
  )

  container.start()

  val mappedPort = container.mappedPort(mongoPort)
  val containerIp = container.containerIpAddress

  println(s"mappedPort: ${mappedPort}, containerIp: ${containerIp}")
  implicit override def newAppForTest(testData: TestData): Application =
    new GuiceApplicationBuilder()
      .configure("db.user" -> dbUser)
      .configure("db.password" -> dbPassword)
      .configure("db.name" -> dbName)
      .configure("db.host" -> s"${containerIp}:${mappedPort}")
      .build()

  val testId = new ObjectId().toString
  val jwtToken    = readFileAsString("/jwt_token.txt")
  val testUserId = "6" // taken from jwt_token.txt
  val anotherUserId = "2123"
  val bearerToken = s"Bearer ${jwtToken}"

  val testCategory = CategoryResource(Some(testId), testUserId, "description", None)

  val categories = Vector(
    CategoryResource(Some(testId), testUserId, "", None),
    CategoryResource(None, testUserId, "", None),
    CategoryResource(None, testUserId, "", None),
    CategoryResource(None, testUserId, "", None),
    CategoryResource(None, testUserId, "", None)
  )

  def listPath(userId: String)   = s"/api/finance/users/${userId}/categories/"
  def showPath(userId: String, id: String)   = s"/api/finance/users/${userId}/categories/${id}/"
  def createPath(userId: String) = listPath(userId)

  "CategoryRouter" should {

    "not allow unauthorized requests" in {
      val request = FakeRequest(GET, s"${listPath(testUserId.toString)}")
        .withHeaders(HOST -> "localhost:9000")
        .withCSRFToken
      val home: Future[Result] = route(app, request).get
      val response             = await(home)
      response.header.status mustBe FORBIDDEN
    }

    "not allow indexing other users accounts" in {
      val request = FakeRequest(GET, s"${listPath(anotherUserId.toString)}")
        .withHeaders(HOST -> "localhost:9000", AUTHORIZATION -> bearerToken)
        .withCSRFToken
      val home: Future[Result] = route(app, request).get
      val response             = await(home)
      response.header.status mustBe FORBIDDEN
    }

    "not allow showing other users' accounts" in {
      val request = FakeRequest(GET, s"${showPath(anotherUserId.toString, "someId")}")
        .withHeaders(HOST -> "localhost:9000", AUTHORIZATION -> bearerToken)
        .withCSRFToken
      val home: Future[Result] = route(app, request).get
      val response             = await(home)
      response.header.status mustBe FORBIDDEN
    }

    "not allow creating categories for other users" in {
      val category = Category("Skatt", None, None)
      val request = FakeRequest(POST, s"${createPath(anotherUserId.toString)}")
        .withHeaders(HOST -> "localhost:9000", AUTHORIZATION -> bearerToken)
        .withJsonBody(Json.toJson(category))
        .withCSRFToken
      val home: Future[Result] = route(app, request).get
      val response             = await(home)
      response.header.status mustBe FORBIDDEN
    }

    "create a Category" in {
      val input = Category("Skatt", Some(testUserId), None)
      val id    = createCategory(input)
      (id == "") mustBe false
    }

    "get a Category" in {
      val createInput = Category("Hyra", Some(testUserId), None)
      val id          = createCategory(createInput)
      (id == "") mustBe false

      val getRequest = FakeRequest(GET, showPath(testUserId.toString, id))
        .withHeaders(HOST -> "localhost:9000", AUTHORIZATION -> bearerToken)
        .withCSRFToken
      val get: Future[Result] = route(app, getRequest).get

      val category: CategoryResource =
        Json.fromJson[CategoryResource](contentAsJson(get)).get

      category.id mustBe (Some(id))
      category.userId mustBe testUserId
      category.description mustBe createInput.description
      category.parentCategoryId mustBe createInput.parentCategoryId
    }

    "list Categories" in {
      val createInput0 = Category("Hyra", Some(testUserId), None)
      val id0          = createCategory(createInput0)

      val createInput1 = Category("Övrigt", Some(testUserId), None)
      val id1          = createCategory(createInput1)

      val getRequest = FakeRequest(GET, listPath(testUserId.toString))
        .withHeaders(HOST -> "localhost:9000", AUTHORIZATION -> bearerToken)
        .withCSRFToken
      val get: Future[Result] = route(app, getRequest).get

      val categories: Seq[CategoryResource] =
        Json.fromJson[Seq[CategoryResource]](contentAsJson(get)).get
      println(categories)
      categories.size >= 2
      val maybeCategory0 = categories.find { _.id == Some(id0) }
      val maybeCategory1 = categories.find { _.id == Some(id1) }

      maybeCategory0.nonEmpty mustBe true
      maybeCategory1.nonEmpty mustBe true

      val category0 = maybeCategory0.get
      val category1 = maybeCategory1.get

      category0.id mustBe Some(id0)
      category0.userId mustBe createInput0.userId.get
      category0.description mustBe createInput0.description
      category0.parentCategoryId mustBe createInput0.parentCategoryId

      category1.id mustBe Some(id1)
      category1.userId mustBe createInput1.userId.get
      category1.description mustBe createInput1.description
      category1.parentCategoryId mustBe createInput1.parentCategoryId
    }

    "not allow a trailing slash for get" in {
      val request = FakeRequest(GET, "/api/categories/1")
        .withHeaders(HOST -> "localhost:9000")
        .withCSRFToken
      val home: Future[Result] = route(app, request).get

      val response = await(home)
      response.header.status mustBe NOT_FOUND
    }

    "not allow a trailing slash for list" in {
      val request = FakeRequest(GET, "/api/categories")
        .withHeaders(HOST -> "localhost:9000", AUTHORIZATION -> bearerToken)
        .withCSRFToken
      val home: Future[Result] = route(app, request).get

      val response = await(home)
      response.header.status mustBe NOT_FOUND
    }
  }

  private def createCategory(category: Category): String = {
    val createRequest = FakeRequest(POST, createPath(testUserId.toString))
      .withHeaders(HOST -> "localhost:9000", AUTHORIZATION -> bearerToken)
      .withJsonBody(Json.toJson(category))
      .withCSRFToken
    val create: Future[Result] = route(app, createRequest).get

    val createResponse = await(create)
    createResponse.header.status mustBe CREATED
    val id = Json.fromJson[String](contentAsJson(create)).get
    id
  }

  def readFileAsString(path: String): String = {
    val uri = getClass.getResource(path).toURI()
    new String(java.nio.file.Files.readAllBytes(Paths.get(uri)), UTF_8)
  }

}
