package v1.transaction

import play.api.libs.json.{JsResult, Json}
import org.mockito.{ArgumentMatchers, Matchers, Mockito}
import org.scalatest.TestData
import org.scalatestplus.mockito._
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.mvc.Result
import play.api.test.CSRFTokenHelper._
import play.api.test.Helpers._
import play.api.test._

import scala.concurrent.Future
import scala.language.implicitConversions

import v1.category._
import v1.transaction._
import org.apache.commons.io.IOUtils
import java.nio.charset.StandardCharsets
import play.api.mvc.MultipartFormData
import play.api.libs.Files.TemporaryFile
import play.api.mvc.MultipartFormData.FilePart
import play.api.mvc.MultipartFormData.BadPart
import play.api.libs.Files
import java.io.File
import play.api.libs.Files.SingletonTemporaryFileCreator
import java.nio.file.Paths
import java.nio.charset.StandardCharsets.UTF_8
import java.util.UUID
import org.mongodb.scala.bson.ObjectId
import com.dimafeng.testcontainers._
import org.testcontainers.containers.wait.strategy._

class TransactionRouterSpec extends PlaySpec with GuiceOneAppPerTest with MockitoSugar {


  val mongoPort = 27017

  val dbUser = "finance_test_app"
  val dbPassword = "finance_test_pw"
  val dbName = "finance_test_db"

  val container = GenericContainer(
    "bitnami/mongodb:latest",
    exposedPorts = Seq(mongoPort),
    waitStrategy = Wait.forHttp("/"),
    env = Map("MONGODB_USERNAME" -> dbUser,
              "MONGODB_PASSWORD" -> dbPassword,
              "MONGODB_DATABASE" -> dbName
    )
  )

  container.start()


  val mappedPort = container.mappedPort(mongoPort)
  val containerIp = container.containerIpAddress

  println(s"mappedPort: ${mappedPort}, containerIp: ${containerIp}")
  implicit override def newAppForTest(testData: TestData): Application =
    new GuiceApplicationBuilder()
      .configure("db.user" -> dbUser)
      .configure("db.password" -> dbPassword)
      .configure("db.name" -> dbName)
      .configure("db.host" -> s"${containerIp}:${mappedPort}")
      .build()

  val testId = new ObjectId().toString
  val jwtToken    = readFileAsString("/jwt_token.txt")
  val testUserId = "6" // taken from jwt_token.txt
  val anotherUserId = "1123"
  val bearerToken = s"Bearer ${jwtToken}"

  val testTransaction = TransactionResource(Some(testId), testUserId, None, None, 100, "", "", "SEK", None)

  val transactions = Vector(
    TransactionResource(Some(testId), testUserId, None, Some("3"), 100, "", "", "SEK", None),
    TransactionResource(None, testUserId, None, None, 100, "", "", "SEK", None),
    TransactionResource(None, testUserId, None, None, 100, "", "", "SEK", None),
    TransactionResource(None, testUserId, None, None, 100, "", "", "SEK", None),
    TransactionResource(None, testUserId, None, None, 100, "", "", "SEK", None)
  )

  private def listPath(userId: String)   = s"/api/finance/users/${userId}/transactions/"
  private def showPath(userId: String, transactionId: String)   = s"${listPath(userId)}$transactionId/"
  private def createPath(userId: String) = listPath(userId)
  private def createCsvPath(userId: String) = s"/api/finance/users/${userId}/transactionFiles/"

  "TransactionRouter" should {

    "not allow unauthorized requests" in {
      val request = FakeRequest(GET, listPath(testUserId))
        .withHeaders(HOST -> "localhost:9000")
        .withCSRFToken
      val home: Future[Result] = route(app, request).get
      val response             = await(home)
      response.header.status mustBe FORBIDDEN
    }

    "not allow indexing other users transactions" in {
      val request = FakeRequest(GET, s"${listPath(anotherUserId.toString)}")
        .withHeaders(HOST -> "localhost:9000", AUTHORIZATION -> bearerToken)
        .withCSRFToken
      val home: Future[Result] = route(app, request).get
      val response             = await(home)
      response.header.status mustBe FORBIDDEN
    }

    "not allow showing other users' transactions" in {
      val request = FakeRequest(GET, s"${showPath(anotherUserId.toString, "someId")}")
        .withHeaders(HOST -> "localhost:9000", AUTHORIZATION -> bearerToken)
        .withCSRFToken
      val home: Future[Result] = route(app, request).get
      val response             = await(home)
      response.header.status mustBe FORBIDDEN
    }

    "not allow creating transactions for other users" in {
      val transaction = Transaction(100, "2019-01-01", "blabla", "SEK", None, None)
      val request = FakeRequest(POST, s"${listPath(anotherUserId.toString)}")
        .withHeaders(HOST -> "localhost:9000", AUTHORIZATION -> bearerToken)
        .withJsonBody(Json.toJson(transaction))
        .withCSRFToken
      val home: Future[Result] = route(app, request).get
      val response             = await(home)
      response.header.status mustBe FORBIDDEN
    }

    "create a Transaction" in {
      val input = Transaction(100, "2019-01-01", "blabla", "SEK", None, None)
      val id    = createTransaction(input)
      (id == "") mustBe false
    }

    "create a Transaction with a categoryId" in {
      val createInput = Transaction(100, "2019-01-01", "blabla", "SEK", None, Some("123"))
      val id          = createTransaction(createInput)
      (id == "") mustBe false

      val getRequest = FakeRequest(GET, showPath(testUserId, id))
        .withHeaders(HOST -> "localhost:9000", AUTHORIZATION -> bearerToken)
        .withCSRFToken
      val get: Future[Result] = route(app, getRequest).get

      val transaction: TransactionResource =
        Json.fromJson[TransactionResource](contentAsJson(get)).get
      println(transaction)
      transaction.id mustBe (Some(id))
      transaction.amount mustBe (createInput.amount)
      transaction.date mustBe (createInput.date)
      transaction.text mustBe (createInput.text)
      transaction.currency mustBe (createInput.currency)
      transaction.userId mustBe testUserId
      transaction.categoryId mustBe createInput.categoryId
    }

    "create Transactions from a csv file" in {
      val resource = getClass.getResource("/transactions.csv")
      val uri      = resource.toURI()
      val file     = SingletonTemporaryFileCreator.create(Paths.get(uri))
      val part = FilePart[TemporaryFile](
        key = "thekey",
        filename = uri.getPath(),
        contentType = None,
        ref = file
      )

      val files         = Seq[FilePart[TemporaryFile]](part)
      val multipartBody = MultipartFormData(Map[String, Seq[String]](), files, Seq[BadPart]())

      val request = FakeRequest(POST, createCsvPath(testUserId))
        .withHeaders(HOST -> "localhost:9000", AUTHORIZATION -> bearerToken)
        .withMultipartFormDataBody(multipartBody)
        .withCSRFToken
      val home: Future[Result] = route(app, request).get

      val response = await(home)
      response.header.status mustBe CREATED
      val transactions = Json.fromJson[Seq[Transaction]](contentAsJson(home)).get
      transactions.size.mustBe(9)
      transactions.head.userId.mustBe(Some(testUserId))
    }

    "show a Transaction" in {
      val createInput = Transaction(100, "2019-01-01", "blabla", "SEK", None, None)
      val id          = createTransaction(createInput)
      (id == "") mustBe false

      val getRequest = FakeRequest(GET, showPath(testUserId, id))
        .withHeaders(HOST -> "localhost:9000", AUTHORIZATION -> bearerToken)
        .withCSRFToken
      val get: Future[Result] = route(app, getRequest).get

      val transaction: TransactionResource =
        Json.fromJson[TransactionResource](contentAsJson(get)).get
      println(transaction)
      transaction.id mustBe (Some(id))
      transaction.amount mustBe (createInput.amount)
      transaction.date mustBe (createInput.date)
      transaction.text mustBe (createInput.text)
      transaction.currency mustBe (createInput.currency)
      transaction.userId mustBe testUserId
    }

    "list Transactions" in {
      val createInput0 = Transaction(40000, "2012-01-01", "text 0", "EUR", None, None)
      val id0          = createTransaction(createInput0)

      val createInput1 = Transaction(18189, "2017-01-01", "text 1", "SEK", None, None)
      val id1          = createTransaction(createInput1)

      val getRequest = FakeRequest(GET, listPath(testUserId))
        .withHeaders(HOST -> "localhost:9000", AUTHORIZATION -> bearerToken)
        .withCSRFToken
      val get: Future[Result] = route(app, getRequest).get

      val transactions: Seq[TransactionResource] =
        Json.fromJson[Seq[TransactionResource]](contentAsJson(get)).get
      println(transactions)
      transactions.size >= 2
      val maybeTransaction0 = transactions.find { _.id == Some(id0) }
      val maybeTransaction1 = transactions.find { _.id == Some(id1) }

      maybeTransaction0.nonEmpty mustBe true
      maybeTransaction1.nonEmpty mustBe true

      val transaction0 = maybeTransaction0.get
      val transaction1 = maybeTransaction1.get

      transaction0.amount mustBe createInput0.amount
      transaction0.date mustBe createInput0.date
      transaction0.text mustBe createInput0.text
      transaction0.currency mustBe createInput0.currency
      transaction0.userId mustBe testUserId

      transaction1.amount mustBe createInput1.amount
      transaction1.date mustBe createInput1.date
      transaction1.text mustBe createInput1.text
      transaction1.currency mustBe createInput1.currency
      transaction1.userId mustBe testUserId
    }

    "not allow a trailing slash for get" in {
      val request = FakeRequest(GET, "/api/transactions/1")
        .withHeaders(HOST -> "localhost:9000")
        .withCSRFToken
      val home: Future[Result] = route(app, request).get

      val response = await(home)
      response.header.status mustBe NOT_FOUND
    }

    "not allow a trailing slash for list" in {
      val request = FakeRequest(GET, "/api/transactions")
        .withHeaders(HOST -> "localhost:9000", AUTHORIZATION -> bearerToken)
        .withCSRFToken
      val home: Future[Result] = route(app, request).get

      val response = await(home)
      response.header.status mustBe NOT_FOUND
    }
  }

  private def createTransaction(transaction: Transaction): String = {
    val createRequest = FakeRequest(POST, createPath(testUserId))
      .withHeaders(HOST -> "localhost:9000", AUTHORIZATION -> bearerToken)
      .withJsonBody(Json.toJson(transaction))
      .withCSRFToken
    val create: Future[Result] = route(app, createRequest).get

    val createResponse = await(create)
    createResponse.header.status mustBe CREATED
    val id = Json.fromJson[String](contentAsJson(create)).get
    id
  }

  def readFileAsString(path: String): String = {
    val uri = getClass.getResource(path).toURI()
    new String(java.nio.file.Files.readAllBytes(Paths.get(uri)), UTF_8)
  }

}
